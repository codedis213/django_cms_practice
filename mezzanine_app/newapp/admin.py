from django.contrib import admin

# Register your models here.
from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from copy import deepcopy
from .models import Author, Book, DocPage

author_extra_fieldsets = ((None, {"fields": ("dob",)}),)

class BookInline(admin.TabularInline):
    model = Book

class AuthorAdmin(PageAdmin):
    inlines = (BookInline,)
    fieldsets = deepcopy(PageAdmin.fieldsets) + author_extra_fieldsets

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book)

admin.site.register(DocPage, PageAdmin)


