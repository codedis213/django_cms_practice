from django.shortcuts import render

# Create your views here.

from django.views.generic import ListView

from .models import Post


class PostListView(ListView):

    template_name = "newapp/post_list.html"

    def get_queryset(self):
        return Post.objects.all()
