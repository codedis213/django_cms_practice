from __future__ import unicode_literals

from django.db import models

# Create your models here.from django.db import models
from mezzanine.pages.models import Page

# The members of Page will be inherited by the Author model, such
# as title, slug, etc. For authors we can use the title field to
# store the author's name. For our model definition, we just add
# any extra fields that aren't part of the Page model, in this
# case, date of birth.

class Author(Page):
	# title = models.CharField(max_length=100)
	dob = models.DateField(auto_now_add=True)

class Book(models.Model):
    author = models.ForeignKey("Author")
    cover = models.ImageField(upload_to="authors")

from django.utils.translation import ugettext_lazy as _
from mezzanine.pages.models import Page, RichText

class DocPage(Page, RichText):
    """
    A doc tree page
    """
    add_toc = models.BooleanField(_("Add TOC"), default=False,
                                  help_text=_("Include a list of child links"))

    class Meta:
        verbose_name = _("Doc Page")
        verbose_name_plural = _("Doc Pages")


"""
The main purpose of these models is to do manual testing of
the mongonaut front end.  Do not use this code as an actual blog
backend.
"""

from datetime import datetime

from mongoengine import BooleanField
from mongoengine import DateTimeField
from mongoengine import Document
from mongoengine import EmbeddedDocument
from mongoengine import EmbeddedDocumentField
from mongoengine import ListField
from mongoengine import ReferenceField
from mongoengine import StringField


class User(Document):
    email = StringField(required=True, max_length=50)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)

    meta = {'allow_inheritance': True}

    def __unicode__(self):
        return self.email

class NewUser(User):
    new_field = StringField()

    def __unicode__(self):
        return self.email


class Comment(EmbeddedDocument):
    message = StringField(default="DEFAULT EMBEDDED COMMENT")
    author = ReferenceField(User)

    # ListField(EmbeddedDocumentField(ListField(Something)) is not currenlty supported.
    # UI, and lists with list inside them need to be fixed.  The extra numbers appened to
    # the end of the key and class need to happen correctly.
    # Files to fix: list_add.js, forms.py, and mixins.py need to be updated to work.
    # likes = ListField(ReferenceField(User))


class EmbeddedUser(EmbeddedDocument):
    email = StringField(max_length=50, default="default-test@test.com")
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    created_date = DateTimeField()  # Used for testing
    is_admin = BooleanField()  # Used for testing
    # embedded_user_bio = EmbeddedDocumentField(Comment)
    friends_list = ListField(ReferenceField(User))

    # Not supportted see above comment on Comment
    # user_comments = ListField(EmbeddedDocumentField(Comment))


class Post(Document):
    # See Post.title.max_length to make validation better!
    title = StringField(max_length=120, required=True, unique=True)
    content = StringField(default="I am default content")
    author = ReferenceField(User, required=True)
    created_date = DateTimeField()
    published = BooleanField()
    creator = EmbeddedDocumentField(EmbeddedUser)
    published_dates = ListField(DateTimeField())
    tags = ListField(StringField(max_length=30))
    past_authors = ListField(ReferenceField(User))
    comments = ListField(EmbeddedDocumentField(Comment))

    def save(self, *args, **kwargs):
        if not self.created_date:
            self.created_date = datetime.utcnow()
            if not self.creator:
                self.creator = EmbeddedUser()
                self.creator.email = self.author.email
                self.creator.first_name = self.author.first_name
                self.creator.last_name = self.author.last_name
        if self.published:
            self.published_dates.append(datetime.utcnow())
        super(Post, self).save(*args, **kwargs)


