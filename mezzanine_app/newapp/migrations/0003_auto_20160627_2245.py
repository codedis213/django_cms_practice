# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-27 22:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newapp', '0002_docpage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='dob',
            field=models.DateField(auto_now_add=True),
        ),
    ]
